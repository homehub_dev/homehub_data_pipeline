# Intro
This is offline pipeline for HomeHub project. It's main purposes:
- vector tiles generation
- building of search indices for geo-search service

This is a classit ETL-pipeline: it downloads OSM data, clips it according to cities borders, loads to postgres,
modifies, enriches, cooks vector tiles and search indices from it.

The resulting data (tiles and search indices) is used on the main page of homehub.su. Also it is used locally for debugging purposes.

# Prerequisites
To run pipeline you need to install docker and docker-compose.
We tested our pipeline only on Linux with:
- Docker version 20.10.6-ce
- docker-compose version 1.27.4 or 1.29.2

# Steps before first run
Clone the pipeline repo:
```
git clone https://gitlab.com/homehub_dev/homehub_data_pipeline.git
```

Ensure that file ```supported_cities.json``` contains information about cities you want to process:
```
cat homehub_data_pipeline/run_pipeline/data/supported_cities.json
```

Go to the directory with docker-compose configurations. You will run all pipeline tasks from this directory:
```
cd homehub_data_pipeline/run_pipeline
```

Build and run configuration for creating cities borders. These borders will be used later for clipping osm.pbf files, determining areas around cities which is rendered on map, etc:
```
docker-compose -f docker-compose-city-borders.yml build && \
docker-compose -f docker-compose-city-borders.yml up
```

After this step cities borders in geojson format are stored in ```run_pipeline/data```.

The next step is to download latest OSM data and clip it according to prepared borders:
```
docker-compose -f docker-compose-download-osm.yml build && docker-compose -f docker-compose-download-osm.yml up
```

After this step osm.pbf files for all declared cities appear in ```run_pipeline/data```.


# Подпроекты
- run_pipeline - docker-compose, через который запускается весь пайплайн от скачивания данных из веба до нарезки тайлов. Почти все остальные подпроекты - образы, которые в нем запускаются.
- maps_db - БД PostgreSQL с табличками, из которых в итоге варятся тайлы. Не только "дороги", "зеленые зоны" и прочие ОСМовые выжимки, но и вычисляемые POI экологии, гексагончики для генерализации показателей.
- import_osm_to_maps_db - импортер imposm3 из OSM pbf в таблички PostgreSQL.
- import_geojson_to_maps_db - так как вытаскивать costline из OSM - дорогостоящая операция, требущая наличия всей планеты, хорошей идеей показалось просто взять из NaturalEarth необходимые выжимки. Этот подпроект сохраняет данные о береговой линии, основных городах, странах и тд в PostgreSQL.
- prepare_meta_data - пока что мы отображаем на карте только один город. Планируем отображать несколько. Чтобы делать это предельно изяшно, было решено обрезать данные в некотором радиусе вокруг центроида города. Этот подпроект генерит geojson с геометрией круга.
- prepare_osm_data - подпроект для обрезки pbf из OSM по окружности, полученной в prepare_meta_data.
- seed_tiles - магия по расчету данных для слоев и варки тайлов.
- seed_dem_raster_tiles - генерация растровых тайлов для слоя "рельеф". Запускается редко, если данные с предыдущей генерации куда-то задевались.
- helpers - здесь лежит генератор шрифтов и образ spritezero-docker для склеивания спрайтов из иконок для Mapbox GL JS. Глючен и забагован, почти всегда всю работу надо делать за него.

# Запуск
В подпроекте run_pipeline есть несколько docker-compose конфигов: какие-то из многоконтейнерные приложения используются гораздо чаще, чем другие.
- docker-compose-dem.yml запускается единожды. Он берет растровые тайлы из датасета SRTM v3, которые неизменны годами. Результат работы: директория seed_dem_raster_tiles/work_dir/relief, в которой содержатся jpeg-тайлы рельефа.
- docker-compose-city-borders.yml должен отработать хотя бы раз. Дальше запускается при добавлении новых городов на карту или модификации их границ. 
- docker-compose-download-osm.yml запускается для скачивания OSM-файлов, то есть всякий раз для обновления данных.
- docker-compose-generate-tiles.yml запускается для переобсчета данных и пересборки тайлов. В нем данные из OSM попадают в бд и превращаются в тайлы.

Удобнее всего запускать отдельные шаги последовательно, один за другим в detached-режиме:
```
cd run_pipeline
docker-compose -f docker-compose-some-step.yml build
docker-compose -f docker-compose-some-step.yml up -d
```

Эта команда соберет и запустит все необходимые контейнеры.

Проверить, какие сервисы запущены, можно так:
```
docker ps
```

Просмотр логов конкретного контейнера:
```
docker logs %CONTAINER_ID% -f
```

Остановка сервисов:
```
docker-compose down
```

Для полной перезаписи бд нужно грохнуть volume. Так можно убрать все volume:
```
docker rm -f $(docker ps -a -q)
docker volume rm $(docker volume ls -q)
```

Команда `docker-compose up` не собирает новые образы, поэтому если в них были внесены изменения и нужно создать свежий :latest, сначала вызывается команда `docker-compose build`. 

О том, как работать с postgres, можно почитать в maps_db/README.md.
О том, что делает imposm3, есть в import_to_maps_db/README.md.

# Настройка локального окружения для клиентской части
Запустить nginx. Подложить ему конфиг nginx.conf. В конфиге вместо $project_dir указать полный путь к проекту на своей тачке.
Также желательно раскомментировать строки с логами и задать там существующие пути.

После того как nginx запустится, открывать localhost через браузер.
