# Service description

Runs docker container with valhalla service on 8002 exposed port.

# How to run

1. Prepare data directory: copy valhalla_service binary and val_data directory from antitail/routing project (homehub_routing).
There should be config file for valhalla in val_data and valhalla_tiles subdirectory with tiles.
   
2. Run run_docker.sh. It starts service with corresponding configuration and routing tiles.

