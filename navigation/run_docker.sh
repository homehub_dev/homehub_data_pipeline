#!/bin/bash

docker build -t antitail/routing_service:latest .
docker run \
-v /home/ail/homehub_data_pipeline/run_pipeline/data/navigation/data:/service_dir \
-p 8002:8002 antitail/routing_service:latest
