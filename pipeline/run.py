import argparse
import os
from pathlib import Path
from typing import Dict
from typing import List
import time

from pipeline.services.compute_manager import StageComputeManager
from pipeline.services.internal import Stage
from pipeline.services.status_storage import FileStageStatusStorage
from pipeline.stages.build_search_index import run as build_search_index
from pipeline.stages.create_ecology_coverage import run as create_ecology_coverage
from pipeline.services.entities import HexagonSize
from pipeline.stages.create_infrastructure_coverage import run as create_infrastructure_coverage
from pipeline.stages.create_filters_coverage import run as create_filters_coverage
from pipeline.stages.create_life_quality_coverage import run as create_life_quality_coverage
from pipeline.stages.fix_osm_data import run as fix_osm_data
from pipeline.stages.fix_tables_for_search_index import run as fix_tables_for_search_index
from pipeline.stages.import_geojson_to_maps_db import run as import_geojson_to_maps_db
from pipeline.stages.import_osm_to_maps_db import run as import_osm_to_maps_db
from pipeline.stages.prepare_tables import run as prepare_tables
from pipeline.stages.seed_tiles import run as seed_tiles


def get_cities_list(s):
    cities = s.lower().strip().split(',')
    if len(cities) == 1 and cities[0] == "all":
        return []


def cli():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--osm-dir', required=True, default=Path('data'),
                        help='Output directory for downloaded files.')

    parser.add_argument('--cities', required=True, default="",
                        help="List of cities separated by commas.")

    parser.add_argument('--from-stage', required=False, default=None,
                        help='From stage')

    parser.add_argument('--to-stage', required=False, default=None,
                        help="To stage")

    parser.add_argument('--continue', required=False, action='store_true', default=argparse.SUPPRESS,
                        help="Continue running pipeline from last successful finished stage")

    args = parser.parse_args()
    osm_dir = Path(args.osm_dir)
    if osm_dir.is_file():
        raise AssertionError(f"{osm_dir} is file, not directory")
    osm_dir.mkdir(exist_ok=True, parents=True)

    root_tiles_path = osm_dir / 'tiles'
    root_tiles_path.mkdir(exist_ok=True, parents=True)

    cities = get_cities_list(args.cities)
    # Sleep before postgres container is set up:
    time.sleep(2*60)
    compute_manager = startup_compute_manager(osm_dir, root_tiles_path, cities)
    if hasattr(args, 'continue'):
        compute_manager.compute_from_last_successful_stage()
    else:
        compute_manager.compute(args.from_stage, args.to_stage)


def get_stages(
        resources_folder: Path,
        osm_dir: Path,
        root_tiles_path: Path,
        cities: List[str]
) -> List[Stage]:
    database_url = os.getenv("DATABASE_URL", "postgres://postgres:postgres_pass@localhost:65432/maps_db")
    imposm_proto = os.getenv("IMPOSM_PROTO", "postgis://")
    psycopg2_proto = os.getenv("PSYCOPG2_PROTO", "postgresql://")

    navigation_url = os.getenv("NAVIGATION_URL", "http://localhost:8002/sources_to_targets")

    import_osm_to_maps_db_stage = Stage(
        order_number=0,
        name='import_osm_to_maps_db',
        description='',
        execution_method=import_osm_to_maps_db.run_stage,
        execution_args=(imposm_proto+database_url, str(resources_folder / 'import_osm_to_maps_db'), osm_dir, cities)
    )

    db_conn_str = psycopg2_proto + database_url

    prepare_tables_stage = Stage(
        order_number=1,
        name='prepare_tables',
        description='',
        execution_method=prepare_tables.run_stage,
        execution_args=(db_conn_str,)
    )

    import_geojson_to_maps_db_stage = Stage(
        order_number=2,
        name='import_geojson_to_maps_db',
        description='',
        execution_method=import_geojson_to_maps_db.run_stage,
        execution_args=(db_conn_str, str(resources_folder / 'import_geojson_to_maps_db'))
    )

    fix_osm_data_stage = Stage(
        order_number=3,
        name='fix_osm_data',
        description='',
        execution_method=fix_osm_data.run_stage,
        execution_args=(db_conn_str, osm_dir)
    )

    base_hexagon_edge_length_m = 100.
    hexagons_edge_sizes: Dict[HexagonSize, float] = {
        HexagonSize.base: base_hexagon_edge_length_m,
        HexagonSize.s: base_hexagon_edge_length_m * 2,
        HexagonSize.m: base_hexagon_edge_length_m * 4,
        HexagonSize.l: base_hexagon_edge_length_m * 8,
        HexagonSize.xl: base_hexagon_edge_length_m * 10,
        HexagonSize.xxl: base_hexagon_edge_length_m * 12,
        HexagonSize.xxxl: base_hexagon_edge_length_m * 14,
        HexagonSize.xxxxl: base_hexagon_edge_length_m * 16,
        HexagonSize.xxxxxl: base_hexagon_edge_length_m * 18
    }
    ecology_r_m = 2000.
    grid_cell_side_m = 200.
    segment_length_m = 10.
    sector_size_m = 3000.
    create_ecology_coverage_stage = Stage(
        order_number=4,
        name='create_ecology_coverage',
        description='',
        execution_method=create_ecology_coverage.run_stage,
        execution_args=(
            db_conn_str,
            hexagons_edge_sizes,
            ecology_r_m,
            grid_cell_side_m,
            segment_length_m,
            sector_size_m,
            osm_dir,
            cities
        )
    )

    create_infrastructure_coverage_stage = Stage(
        order_number=5,
        name='create_infrastructure_coverage',
        description='',
        execution_method=create_infrastructure_coverage.run_stage,
        execution_args=(db_conn_str, navigation_url)
    )

    create_filters_coverage_stage = Stage(
        order_number=6,
        name='create_filters_coverage',
        description='Fills fields in hexagon tables which correspond to subway proximity and other params',
        execution_method=create_filters_coverage.run_stage,
        execution_args=(
            db_conn_str, 
            navigation_url,
            osm_dir,
            cities
        )
    )

    create_life_quality_coverage_stage = Stage(
        order_number=7,
        name='create_life_quality_coverage',
        description='Fills fields in hexagon tables which correspond to life_quality',
        execution_method=create_life_quality_coverage.run_stage,
        execution_args=(
            db_conn_str, 
            osm_dir,
            cities
        )
    )

    seed_tiles_stage = Stage(
        order_number=8,
        name='seed_tiles',
        description='',
        execution_method=seed_tiles.run_stage,
        execution_args=(db_conn_str, osm_dir, root_tiles_path)
    )

    fix_tables_for_search_index_stage = Stage(
        order_number=9,
        name='fix_tables_for_search_index',
        description='',
        execution_method=fix_tables_for_search_index.run_stage,
        execution_args=(db_conn_str,)
    )

    build_search_index_stage = Stage(
        order_number=10,
        name='build_search_index',
        description='',
        execution_method=build_search_index.run_stage,
        execution_args=(db_conn_str, osm_dir)
    )

    all_stages = [
        import_osm_to_maps_db_stage,
        prepare_tables_stage,
        import_geojson_to_maps_db_stage,
        fix_osm_data_stage,
        create_ecology_coverage_stage,
        create_infrastructure_coverage_stage,
        create_filters_coverage_stage,
        create_life_quality_coverage_stage,
        fix_tables_for_search_index_stage,
        seed_tiles_stage,
        build_search_index_stage
    ]
    return all_stages


def startup_compute_manager(osm_dir: Path, root_tiles_path: Path, cities: List[str]) -> StageComputeManager:
    resources_folder = Path("resources")
    status_data_folder = Path(os.path.join(osm_dir,"stages_statuses"))
    stages = get_stages(resources_folder, osm_dir, root_tiles_path, cities)
    stage_status_storage = FileStageStatusStorage(status_data_folder)
    stage_compute_manager = StageComputeManager(stage_status_storage, stages)
    return stage_compute_manager


if __name__ == '__main__':
    cli()
