# HomeHub Data Pipeline

This directory contains `python`-packages which uses to 
prepare data for `HomeHub`-project.

## Interpreter

You can use `Python`-interpreter from `Dockerfile`. To build `docker`-image 
use the command below:
```shell
docker build -t homehub_pipeline -f Dockerfile .
```