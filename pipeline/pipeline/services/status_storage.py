from abc import ABC
from abc import abstractmethod
from pathlib import Path

from pipeline.services.internal import StageStatus


class IStageStatusStorage(ABC):
    @abstractmethod
    def get_status_by_stage_name(self, stage_name: str) -> StageStatus:
        pass

    @abstractmethod
    def set_status_by_stage_name(self, stage_name: str, stage_status: StageStatus) -> None:
        pass


class FileStageStatusStorage(IStageStatusStorage):
    def __init__(self, data_folder: Path):
        if data_folder.is_file():
            raise AssertionError(f"{data_folder} is file, not directory")
        data_folder.mkdir(exist_ok=True, parents=True)
        self._data_folder = data_folder

    def get_status_by_stage_name(self, stage_name: str) -> StageStatus:
        if not isinstance(stage_name, str):
            raise ValueError(f'Incorrect stage_name=`{stage_name}`. '
                             f'stage_name must be `str` and not contains whitespaces.')

        filepath = self._data_folder / f'{stage_name.lower()}.status'

        if not filepath.exists():
            return StageStatus.NOT_EXECUTED

        with open(str(filepath), 'r') as file:
            status = file.read()

        return StageStatus(status)

    def set_status_by_stage_name(self, stage_name: str, stage_status: StageStatus) -> None:
        if not isinstance(stage_name, str):
            raise ValueError(f'Incorrect stage_name=`{stage_name}`. '
                             f'stage_name must be `str` and not contains whitespaces.')

        if not isinstance(stage_status, StageStatus):
            raise ValueError(f'Incorrect stage_status value. '
                             f'`StageStatus` was expected but {type(stage_status)} was given')

        filepath = self._data_folder / f'{stage_name.lower()}.status'

        with open(str(filepath), 'w') as file:
            file.write(stage_status.value)
