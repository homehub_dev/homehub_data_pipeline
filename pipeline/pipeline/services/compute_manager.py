from typing import Dict
from typing import List
from typing import Optional
from typing import Set

from pipeline.services.internal import Stage
from pipeline.services.internal import StageStatus
from pipeline.services.status_storage import IStageStatusStorage


class StageComputeManager:
    def __init__(self, stage_status_storage: IStageStatusStorage, stages: List[Stage]):
        self._validate_stages(stages)

        self._stage_status_storage = stage_status_storage
        self._stages = sorted(stages, key=lambda stage: stage.order_number)[:]
        self._stage_name_to_stage: Dict[str, Stage] = {stage.name: stage for stage in stages}

    def _validate_stages(self, stages: List[Stage]):
        if len(stages) == 0:
            raise AssertionError(f"Must be at least one stage in `stages`.")

        stage_names: Set[str] = {stage.name for stage in stages}
        if len(stage_names) != len(stages):
            raise AssertionError("All stages must have different names.")

        if not all(stage.order_number >= 0 for stage in stages):
            raise AssertionError("All order numbers must be non-negative.")

        stages = sorted(stages, key=lambda stage: stage.order_number)[:]
        correct_order = range(len(stages))[:]
        for stage, correct_order_number in zip(stages, correct_order):
            if stage.order_number != correct_order_number:
                raise AssertionError()

    def compute_from_last_successful_stage(self):
        successful_stages = [
            stage
            for stage in self._stages
            if self._stage_status_storage.get_status_by_stage_name(stage.name) is StageStatus.FINISHED_OK
        ]

        if len(successful_stages) == 0:
            from_stage = self._stages[0].name
        else:
            last_successful_stage = successful_stages[-1]
            if last_successful_stage.order_number == len(self._stages) - 1:
                raise ValueError(f"Can`t continue pipeline: "
                                 f"last successful stage `{last_successful_stage.name}` is last in pipeline.")
            from_stage = self._stages[last_successful_stage.order_number + 1].name

        self.compute(from_stage)

    def compute(self, from_stage: Optional[str] = None, to_stage: Optional[str] = None):
        if from_stage is None:
            start_stage = self._stages[0]
        elif from_stage in self._stage_name_to_stage:
            start_stage = self._stage_name_to_stage[from_stage]
        else:
            raise ValueError(f"There is no stage with name=`{from_stage}`")

        if to_stage is None:
            end_stage = self._stages[-1]
        elif to_stage in self._stage_name_to_stage:
            end_stage = self._stage_name_to_stage[to_stage]
        else:
            raise ValueError(f"There is no stage with name=`{to_stage}`")

        if start_stage.order_number > end_stage.order_number:
            raise ValueError(f"`from`-stage={from_stage} must be after `to`-stage={to_stage}.")

        stages_statuses = [
            self._stage_status_storage.get_status_by_stage_name(stage.name)
            for stage in self._stages[:start_stage.order_number]
        ]
        
        # Uncomment if you need all stages to finish successfully:
        #if not all(status is StageStatus.FINISHED_OK for status in stages_statuses):
        #    raise ValueError(f"All stages before `from_stage`=`{from_stage}` must finish successfully.")

        # There is no error in line below. All stages after start-stage become inactual.
        for stage in self._stages[start_stage.order_number:]:
            self._stage_status_storage.set_status_by_stage_name(stage.name, StageStatus.NOT_EXECUTED)

        executable_stages = self._stages[start_stage.order_number:end_stage.order_number + 1]
        for i, stage in enumerate(executable_stages, start=1):
            print(f"Executing stage {i}/{len(executable_stages)}. stage_name=`{stage.name}`.")
            try:
                self._stage_status_storage.set_status_by_stage_name(stage.name, StageStatus.RUNNING)
                # execute stage
                stage.execution_method(*stage.execution_args)

                self._stage_status_storage.set_status_by_stage_name(stage.name, StageStatus.FINISHED_OK)
            except Exception as e:
                self._stage_status_storage.set_status_by_stage_name(stage.name, StageStatus.FINISHED_ERR)
                raise
