from dataclasses import dataclass
from enum import Enum
from typing import Any
from typing import Callable


class StageStatus(Enum):
    NOT_EXECUTED = "NOT_EXECUTED"
    RUNNING = "RUNNING"
    FINISHED_OK = "FINISHED_OK"
    FINISHED_ERR = "FINISHED_ERR"


@dataclass
class Stage:
    order_number: int
    name: str
    description: str
    execution_method: Callable
    execution_args: Any
