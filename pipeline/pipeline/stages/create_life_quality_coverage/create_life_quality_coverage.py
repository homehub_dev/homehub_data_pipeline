import time
from pathlib import Path
from typing import List

import psycopg2
from shapely import wkt

from pipeline.services.utils import get_available_cities
from pipeline.services.entities import City

HEX_TABLE = "hex_tiles"
HEX_SUFFIXES = ["", "_s", "_m", "_l", "_xl"]


def normalize_ecology(val):
    if val is None or val <= 4000:
        return 5.0
    if val <= 8000:
        return 4.0
    if val <= 15000:
        return 3.0
    if val <= 50000:
        return 2.0
    return 1.0

def normalize_bld_dens_pct(val):
    if val is None or val <= 110:
        return 5.0
    if val <= 160:
        return 4.0
    if val <= 180:
        return 3.0
    if val <= 250:
        return 2.0
    return 1.0


def normalize_bld_pct(val):
    if val is None or val <= 25:
        return 5.0
    if val <= 50:
        return 4.0
    if val <= 75:
        return 3.0
    if val <= 90:
        return 2.0
    return 1.0


def normalize_infrastructure(val):
    if val >= 200:
        return 5.0
    if val >= 100:
        return 4.0
    if val >= 50:
        return 3.0
    if val >= 25:
        return 2.0
    return 1.0

def update_hexes_inside_polygon(
        conn,
        table,
        polygon_wgs84):
    query = f"""
        SELECT gid, impact, bld_dens_pct, bld_pct, infrastructure
        FROM {table}
        WHERE ST_Intersects(geometry, ST_Transform('SRID=4326;{polygon_wgs84.wkt}', 3857))
        """

    cursor_upd = conn.cursor()
    cursor = conn.cursor()

    cursor.execute(query)

    for row in cursor.fetchall():
        gid, ecology, bld_dens_pct, bld_pct, infrastructure = row

        ec = normalize_ecology(ecology)
        bdp = normalize_bld_dens_pct(bld_dens_pct)
        bp = normalize_bld_pct(bld_pct)
        ifr = normalize_infrastructure(infrastructure)

        vals = sorted([ec, bdp, bp, ifr])

        life_quality = round(ec*0.3 + bdp*0.3 + bp*0.1 + ifr*0.3) # round((vals[1] + vals[2])/2.0) #
        cursor_upd.execute(f"""UPDATE {table} SET life_quality = {life_quality} WHERE
                        gid = {gid}""")
        print(f"Set life quality = {life_quality} for ecology = {ec}, buildings dens pct = {bdp}, buildings pct = {bp}, infrastructure = {ifr}")
    conn.commit()


def create_coverage(database_url: str, work_dir: Path, cities: List[str]):
    print("Started creating life quality coverage")

    conn = psycopg2.connect(database_url)
    tic = time.perf_counter()

    available_cities: List[City] = get_available_cities(work_dir, cities)

    for i, city in enumerate(available_cities, start=1):
        print(f"Creating life quality for city {city.name}: {i} / {len(available_cities)}")
        for suffix in HEX_SUFFIXES:
            update_hexes_inside_polygon(conn, HEX_TABLE + suffix,  city.polygon_wgs84)

    conn.close()

    toc = time.perf_counter()
    print(f"Finished creating life quality coverage in {toc - tic:0.1f} s")