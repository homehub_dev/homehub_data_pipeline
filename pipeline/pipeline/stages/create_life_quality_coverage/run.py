from pathlib import Path
from typing import List

from pipeline.stages.create_life_quality_coverage.create_life_quality_coverage import create_coverage

def run_stage(
    database_url: str, 
    work_dir: Path,
    cities: List[str]):
    create_coverage(database_url, work_dir, cities)
