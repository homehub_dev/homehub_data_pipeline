from pathlib import Path
from typing import List

from pipeline.stages.create_filters_coverage.create_filters_coverage import create_coverage


def run_stage(
    database_url: str, 
    navigation_url: str,
    work_dir: Path,
    cities: List[str]):
    create_coverage(database_url, navigation_url, work_dir, cities)
