import psycopg2
import time


def fix_tables_for_search_index(database_url: str):
    conn = psycopg2.connect(database_url)
    print("Connected to db")
    tic = time.perf_counter()

    cursor_upd = conn.cursor()
    cursor_upd.execute("INSERT INTO osm_place_poly SELECT * FROM osm_place_point")
    conn.commit()

    if conn:
        conn.close()
    toc = time.perf_counter()

    print(f"Finished fixing places in {toc - tic:0.1f} s")


if __name__ == "__main__":
    fix_tables_for_search_index()
