from pipeline.stages.fix_tables_for_search_index.fix_tables_for_search_index import fix_tables_for_search_index


def run_stage(database_url: str):
    fix_tables_for_search_index(database_url)