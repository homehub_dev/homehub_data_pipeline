from pathlib import Path
from typing import Dict
from typing import List

from pipeline.services.entities import HexagonSize
from pipeline.stages.create_ecology_coverage.pipeline import create_ecology_coverage


def run_stage(
        database_url: str,
        hexagons_edge_sizes: Dict[HexagonSize, float],
        ecology_r_m: float,
        grid_cell_side_m: float,
        segment_length_m: float,
        sector_size_m: float,
        osm_dir: Path,
        cities: List[str]
) -> None:
    create_ecology_coverage(
        database_url,
        hexagons_edge_sizes,
        ecology_r_m,
        grid_cell_side_m,
        segment_length_m,
        sector_size_m,
        osm_dir,
        cities,
    )
