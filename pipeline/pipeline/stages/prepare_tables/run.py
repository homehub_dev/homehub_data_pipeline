from pipeline.stages.prepare_tables.create_tables import create_tables


def run_stage(database_url: str) -> None:
    create_tables(database_url)