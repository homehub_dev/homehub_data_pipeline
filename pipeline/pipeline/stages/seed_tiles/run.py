from pathlib import Path

import pipeline.stages.seed_tiles.seed_tiles as tiles


def run_stage(database_url: str, osm_dir: Path, root_tiles_path: Path):
    tiles.seed_tiles(database_url, osm_dir, root_tiles_path, threads_count=4)
