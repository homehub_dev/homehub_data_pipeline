from pipeline.stages.build_search_index.build_search_index import build_index


def run_stage(database_url: str, destination_dir: str):
    build_index(database_url, destination_dir)