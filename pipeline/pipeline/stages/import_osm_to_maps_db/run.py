# -*- coding: utf-8 -*-

import json
import os
from typing import List


def run_cmd(cmd_str):
    ret_res = os.system(cmd_str)
    if ret_res != 0:
        msg = f"Error {ret_res} running {cmd_str}"
        print(msg)
        raise Exception(msg)


def get_cities_list(s):
    cities = s.lower().strip().split(',')
    if len(cities) == 1 and cities[0] == "all":
        return []


def main(database_url: str, resources_dir: str, osm_dir: str, cities: List[str]):
    with open(os.path.join(osm_dir, 'supported_cities.json'), "r", encoding="utf-8") as json_file:
        supported_cities = json.load(json_file)

    for city in supported_cities["cities"]:
        name = city["title_en"]

        if len(cities) > 0 and name not in cities:
            print("Skipping", name)
            continue

        print("Handling", name)

        path_file = os.path.join(osm_dir, name + ".osm.pbf")
        mapping_path = os.path.join(resources_dir, 'mapping.yaml')
        cmd = f"/go/imposm-0.11.0-linux-x86-64/imposm import " \
              f" -connection {database_url}" \
              f" -read {path_file}" \
              f" -mapping {mapping_path}" \
              f" -write -deployproduction" \
              f" -appendcache"

        run_cmd(cmd)

        print("Loaded", name, "to PostgreSQL")


def run_stage(database_url: str, resources_dir: str, osm_dir: str, cities: List[str]) -> None:
    main(database_url, resources_dir, osm_dir, cities)
