Запускается через docker-compose с конфигом run_pipeline/docker-compose-dem.yml

Берет hgt DEM файлы и генерит из них slippy maps с рельефом.
TODO: выкинуть файлы с данными из git'a, добавить скрипт заливки ASTER или SRTM.

Если нужно перекачать hgt:
Пример url для скачивания ASTER v3
https://e4ftl01.cr.usgs.gov/ASTT/ASTGTM.003/2000.03.01/ASTGTMV003_S77E026.zip

Родительский url для скачивания SRTM v3
https://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/
Ссылки на отдельные тайлы
http://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/N56E038.SRTMGL1.hgt.zip


Результирующие тайлы рельефа местности сохраняются в директорию work_dir/relief.
Они склеиваются из не прозрачного гипсометрического оттенка (рельеф - цвет) и полупрозарчного хилшейда.

Потом их руками нужно скопировать в нужную директорию с тафлами за nginx для раздачи статитки.

Разрешение тайлов:
SRMT file has 1-arcsecond resolution (3601x3601 pixels) in a latitude/longitude projection (EPSG:4326).

Полезные ссылки:
https://tilemill-project.github.io/tilemill/docs/guides/terrain-data/
https://blog.datawrapper.de/shaded-relief-with-gdal-python/