# maps_db
## Описание
Докер-образ с postgres базой данных maps_db. К ней подключены все необходимые расширения для работы с гео-данными.
Пользователь и пароль указаны в переменных окружения докер-файла. 
Таблицы базы должны создаваться и заполняться утилитой imposm из подпроекта import_to_maps_db.

Запускается через docker-compose в связке с другими контейнерами.

## Работа с БД внутри контейнера
К контейнеру можно подключиться и работать в нем с БД через psql:
```
docker exec -it %CONTAINER_ID% bash

psql -U postgres
```

Выход из psql:
```
\q
```

Вывести список всех БД:
```
\l
```

Подключиться к БД:
```
\c maps_db
```

Вывести список расширений БД:
```
\dx
```

Настройки postgres хранятся в служебной БД `pg_settings`. 
Вывести параметры для настройки max_connections:
```
select * from pg_settings where name='max_connections';
```

Обновить настройку:
```
update pg_settings set setting = '100' where name = 'max_connections';
```

## Работа с БД снаружи

Нужно узнать ip докер-контейнера. 
```
docker inspect  %CONTAINER_ID%
```

Команда выведет длинный список параметров, в них встретится что-то вроде
```
"Ports": {
                "5432/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "65432"
                    }
                ]
            },
```

Видно, что ip и порт контейнера, доступные с хоста, - 0.0.0.0 и 65432.

Зная ip, порт, пользователя, пароль и имя БД, к постгресу можно подключаться, например, из питона:

```python
# -*- coding: utf-8 -*-

import psycopg2
conn = psycopg2.connect(dbname='maps_db', user='postgres', 
                        password='postgres_pass', host='0.0.0.0', port="65432")
cursor = conn.cursor()

cursor.execute("SELECT name, admin_level, st_npoints(geometry) FROM osm_boundaries WHERE name ~ 'область' ORDER BY name")
records = cursor.fetchall()
print(records)
cursor.close()
conn.close()
```

Или из консоли, если на хосте установлен psql:
```
psql -h 0.0.0.0 -p 65432 -U postgres
```