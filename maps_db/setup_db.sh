#!/bin/bash
set -e

# Create the 'maps_db' database:
psql -U "$POSTGRES_USER"  -c "CREATE DATABASE maps_db"

# Create PostGIS and other extensitons in maps_db:
psql -U "$POSTGRES_USER" -d maps_db -c "CREATE EXTENSION IF NOT EXISTS postgis"
psql -U "$POSTGRES_USER" -d maps_db -c "CREATE EXTENSION IF NOT EXISTS postgis_topology"
psql -U "$POSTGRES_USER" -d maps_db -c "CREATE EXTENSION IF NOT EXISTS fuzzystrmatch"
psql -U "$POSTGRES_USER" -d maps_db -c "CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder"
psql -U "$POSTGRES_USER" -d maps_db -c "CREATE EXTENSION postgis_sfcgal"

# HStore is a key value store. Implements the hstore data type for storing key-value pairs in a single value.
psql -U "$POSTGRES_USER" -d maps_db -c "CREATE EXTENSION IF NOT EXISTS hstore"

psql -U "$POSTGRES_USER" -d maps_db -a -f postgres_helper_functions.sql